﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ado.net_Exam.Models
{
    public class Street
    {
        public int Id { get; set; }
        public string StreetName { get; set; }
        public int CityId { get; set; }
    }
}