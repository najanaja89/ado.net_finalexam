﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Common;
using ado.net_Exam.Models;

namespace ado.net_Exam.DataAccessLayer
{
    public class DataServices
    {
        private readonly string _connectionString;
        private readonly string _providerName;
        private readonly DbProviderFactory _providerFactory;

        public DataServices()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            _providerName = ConfigurationManager.ConnectionStrings["ConnectionString"].ProviderName;
            _providerFactory = DbProviderFactories.GetFactory(_providerName);

        }

        public List<Country> GetAllCountries()
        {
            var dataCountries = new List<Country>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = "select * from Countries";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var countryName = dataReader["CountryName"].ToString();
                        var id = dataReader["Id"].ToString();
                       
                        dataCountries.Add(new Country
                        {
                            Id = int.Parse(id),
                            CountyName = countryName

                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataCountries;
        }

        //public City FindCityByName(string name)

        public List<City> GetAllCities(int countryId)
        {
            var dataCities = new List<City>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = $"select * from Cities where CountryId={countryId}";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var cityName = dataReader["CityName"].ToString();
                        var id = dataReader["Id"].ToString();

                        dataCities.Add(new City
                        {
                            Id = int.Parse(id),
                            CityName =cityName

                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataCities;
        }


        public List<Street> GetAllStreets(int cityId)
        {
            var dataStreets = new List<Street>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    command.CommandText = $"select * from Streets where CityId={cityId}";

                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var streetName = dataReader["StreetName"].ToString();
                        var id = dataReader["Id"].ToString();

                        dataStreets.Add(new Street
                        {
                            Id = int.Parse(id),
                            StreetName = streetName

                        });
                    }

                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return dataStreets;
        }


        public void AddCountry(string countryName)
         {

            Country country = new Country
            {
                CountyName=countryName
            };

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null;
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    transaction = connection.BeginTransaction();

                    command.CommandText = $"insert into Countries (CountryName) values(@countryName)";
                    command.Transaction = transaction;

                    DbParameter countryNameParameter = command.CreateParameter();
                    countryNameParameter.ParameterName = "@countryName";
                    countryNameParameter.Value = country.CountyName;
                    countryNameParameter.DbType = System.Data.DbType.String;
                    countryNameParameter.IsNullable = false;


                    command.Parameters.AddRange(new DbParameter[] { countryNameParameter });

                    var affectedRows = command.ExecuteNonQuery();

                    if (affectedRows < 1)
                    {
                        throw new Exception("Insert not done");
                    }
                    transaction.Commit();
                    transaction.Dispose();
                }
                catch (DbException exception)
                {

                    //TODO обработка ошибки
                    transaction?.Rollback();
                    transaction.Dispose();
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
        }

        public void AddCity(string cityName, int countryId)
        {

            City city = new City
            {
                CityName = cityName,
                CountryId=countryId
               
            };

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null;
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    transaction = connection.BeginTransaction();

                    command.CommandText = $"insert into Cities (CityName, CountryId) values(@cityName, @countryId)";
                    command.Transaction = transaction;

                    DbParameter cityNameParameter = command.CreateParameter();
                    cityNameParameter.ParameterName = "@cityName";
                    cityNameParameter.Value = city.CityName;
                    cityNameParameter.DbType = System.Data.DbType.String;
                    cityNameParameter.IsNullable = false;

                    DbParameter cityIdParameter = command.CreateParameter();
                    cityIdParameter.ParameterName = "@countryId";
                    cityIdParameter.Value = city.CountryId;
                    cityIdParameter.DbType = System.Data.DbType.Int32;
                    cityIdParameter.IsNullable = false;


                    command.Parameters.AddRange(new DbParameter[] { cityNameParameter, cityIdParameter });

                    var affectedRows = command.ExecuteNonQuery();

                    if (affectedRows < 1)
                    {
                        throw new Exception("Insert not done");
                    }
                    transaction.Commit();
                    transaction.Dispose();
                }
                catch (DbException exception)
                {

                    //TODO обработка ошибки
                    transaction?.Rollback();
                    transaction.Dispose();
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
        }

    }
}
