﻿using ado.net_Exam.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_Exam
{
    public partial class AddCity : Form
    {
        public AddCity(Models.Country country)
        {
            InitializeComponent();
            textBox2.Text = country.Id.ToString();
        }

        private void AddCity_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataServices dataServices = new DataServices();
            dataServices.AddCity(textBox1.Text, int.Parse(textBox2.Text));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
