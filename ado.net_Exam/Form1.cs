﻿using ado.net_Exam.DataAccessLayer;
using ado.net_Exam.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ado.net_Exam
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DataServices dataServices = new DataServices();
            foreach (var item in dataServices.GetAllCountries())
            {
                listBox1.Items.Add(item.CountyName);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //countries
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cities
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            //countries
            DataServices dataServices = new DataServices();
            var country = dataServices.GetAllCountries().Where(x => x.CountyName == listBox1.Text).FirstOrDefault();
           var cities =  dataServices.GetAllCities(country.Id);
            listBox2.Items.Clear();
            foreach (var item in cities)
            {
                listBox2.Items.Add(item.CityName);
            }
            
        }

        private void listBox2_Click(object sender, EventArgs e)
        {
            //Cities
            DataServices dataServices = new DataServices();
            var country = dataServices.GetAllCountries().Where(x => x.CountyName == listBox1.Text).FirstOrDefault();
            var cities = dataServices.GetAllCities(country.Id).Where(x => x.CityName == listBox2.Text).FirstOrDefault();
            var streets = dataServices.GetAllStreets(cities.Id);
            listBox3.Items.Clear();
            foreach (var item in streets)
            {
                listBox3.Items.Add(item.StreetName);
            }
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            //streets
        }

        private void listBox3_Click(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddCountry addCountry = new AddCountry();
            addCountry.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void listBox2_DoubleClick(object sender, EventArgs e)
        {
            DataServices dataServices = new DataServices();
            var country = dataServices.GetAllCountries().Where(x => x.CountyName == listBox1.Text).FirstOrDefault();
       

            AddCity addCity = new AddCity(country);
            addCity.Show();   
        }
    }
}
